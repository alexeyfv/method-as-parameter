```

BenchmarkDotNet v0.13.9+228a464e8be6c580ad9408e98f18813f6407fb5a, Windows 10 (10.0.19045.3448/22H2/2022Update)
Intel Core i5-10310U CPU 1.70GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK 7.0.306
  [Host]     : .NET 6.0.18 (6.0.1823.26907), X64 RyuJIT AVX2
  DefaultJob : .NET 6.0.18 (6.0.1823.26907), X64 RyuJIT AVX2


```
| Method                                                | Mean     | Error    | StdDev    | Median   | Gen0     | Allocated |
|------------------------------------------------------ |---------:|---------:|----------:|---------:|---------:|----------:|
| DelegateAsParameter                                   | 24.11 μs | 0.479 μs |  0.851 μs | 23.84 μs |        - |         - |
| LambdaAsParameter                                     | 33.38 μs | 0.545 μs |  0.483 μs | 33.27 μs |        - |         - |
| LambdaThatCallsInstanceMethodAsParameter              | 85.66 μs | 1.673 μs |  2.233 μs | 85.06 μs | 203.9795 |  640000 B |
| LambdaThatCallsStaticMethodAsParameter                | 30.89 μs | 0.536 μs |  0.475 μs | 30.86 μs |        - |         - |
| AnonymousStaticMethodAsParameter                      | 85.71 μs | 1.714 μs |  2.565 μs | 85.13 μs | 203.9795 |  640000 B |
| AnonymousStaticMethodThatCallsStaticMethodAsParameter | 87.73 μs | 1.736 μs |  3.086 μs | 86.86 μs | 203.9795 |  640000 B |
| AnonymousMethodAsParameter                            | 84.51 μs | 1.491 μs |  1.245 μs | 84.52 μs | 203.9795 |  640000 B |
| AnonymousMethodThatCallsInstanceMethodAsParameter     | 85.64 μs | 1.695 μs |  3.305 μs | 84.30 μs | 203.9795 |  640000 B |
| AnonymousMethodThatCallsStaticMethodAsParameter       | 88.03 μs | 1.757 μs |  4.503 μs | 86.23 μs | 203.9795 |  640000 B |
| InstanceMethodAsParameter                             | 93.24 μs | 5.438 μs | 15.862 μs | 86.15 μs | 203.9795 |  640000 B |
| StaticMethodAsParameter                               | 85.66 μs | 1.670 μs |  2.881 μs | 85.02 μs | 203.9795 |  640000 B |
