using BenchmarkDotNet.Attributes;

[MemoryDiagnoser]
public class BenchmarkableClass
{
    private const int n = 10_000;
    private readonly Func<int, int, int> userFunction;

    public BenchmarkableClass()
    {
        userFunction = InstanceAdd;
    }

    [Benchmark]
    public void DelegateAsParameter()
    {
        for (int i = 0; i < n; i++)
        {
            CallAdd(userFunction, i, i);
        }
    }

    [Benchmark]
    public void LambdaAsParameter()
    {
        for (int i = 0; i < n; i++)
        {
            CallAdd((a, b) => a + b, i, i);
        }
    }

    [Benchmark]
    public void LambdaThatCallsInstanceMethodAsParameter()
    {
        for (int i = 0; i < n; i++)
        {
            CallAdd((a, b) => InstanceAdd(a, b), i, i);
        }
    }
    [Benchmark]
    public void LambdaThatCallsStaticMethodAsParameter()
    {
        for (int i = 0; i < n; i++)
        {
            CallAdd((a, b) => StaticAdd(a, b), i, i);
        }
    }

    [Benchmark]
    public void AnonymousStaticMethodAsParameter()
    {
        static int add(int a, int b) => a + b;

        for (int i = 0; i < n; i++)
        {
            CallAdd(add, i, i);
        }
    }

    [Benchmark]
    public void AnonymousStaticMethodThatCallsStaticMethodAsParameter()
    {
        static int add(int a, int b) => StaticAdd(a, b);

        for (int i = 0; i < n; i++)
        {
            CallAdd(add, i, i);
        }
    }

    [Benchmark]
    public void AnonymousMethodAsParameter()
    {
        int add(int a, int b) => a + b;

        for (int i = 0; i < n; i++)
        {
            CallAdd(add, i, i);
        }
    }

    [Benchmark]
    public void AnonymousMethodThatCallsInstanceMethodAsParameter()
    {
        int add(int a, int b) => InstanceAdd(a, b);

        for (int i = 0; i < n; i++)
        {
            CallAdd(add, i, i);
        }
    }

    [Benchmark]
    public void AnonymousMethodThatCallsStaticMethodAsParameter()
    {
        int add(int a, int b) => StaticAdd(a, b);

        for (int i = 0; i < n; i++)
        {
            CallAdd(add, i, i);
        }
    }

    [Benchmark]
    public void InstanceMethodAsParameter()
    {
        for (int i = 0; i < n; i++)
        {
            CallAdd(InstanceAdd, i, i);
        }
    }

    [Benchmark]
    public void StaticMethodAsParameter()
    {
        for (int i = 0; i < n; i++)
        {
            CallAdd(StaticAdd, i, i);
        }
    }

    private int InstanceAdd(int x, int y) => x + y;

    private static int StaticAdd(int x, int y) => x + y;

    private int CallAdd(Func<int, int, int> add, int arg1, int arg2) => add(arg1, arg2);
}